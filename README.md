# ndp-tcp-fattree-ecmp-datacentre-omnetpp-model

An implementation of NDP[1] protocol (SIGCOMM 2017 Best Paper Award) in OMNeT++/INET. 

[1] NDP: Re-Architecting Datacenter Networks and Stacks for Low Latency https://gianniantichi.github.io/files/papers/ndp.pdf 


A DEMO IS COMING SOON ....................

ndp-tcp-datacentre-omnetpp-model
/samples/inet-myprojects/inet/src/inet/ndp/

NDP Design
![alt text](http://i66.tinypic.com/2jcfp6b.png)

NDP Features 
![alt text](http://i64.tinypic.com/24g36og.png)

NDP in OMNeT++/INET
![alt text](http://i68.tinypic.com/5e7ek4.png)

