#include <omnetpp.h>
#include <string.h>
using namespace std;
using namespace omnetpp;
class Node: public omnetpp::cSimpleModule {

private:
    cMessage *event;  // pointer to the event object which we'll use for timing
   cMessage *cMsg;  // variable to remember the message until we send it back
public:
    Node();
    virtual ~Node();

protected:
    // The following redefined virtual function holds the algorithm.
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

// The module class needs to be registered with OMNeT++
Define_Module(Node);

Node::Node() {
    // Set the pointer to nullptr, so that the destructor won't crash
    // even if initialize() doesn't get called because of a runtime
    // error or user cancellation during the startup process.
    event = cMsg = nullptr;
}

Node::~Node() {
    // Dispose of dynamically allocated the objects
    cancelAndDelete(event);
    delete cMsg;
}

void Node::initialize() {

    // Create the event object we'll use for timing -- just any ordinary message.
    event = new cMessage("eventObjectName");

    // No cMsg message yet.
    cMsg = nullptr;

    if (strcmp("Node1", getName()) == 0) {
        // We don't start right away, but instead send an message to ourselves
        // (a "self-message") -- we'll do the first sending when it arrives back to us, at t=5.0s simulated time.
        cout << "getName() " << getName() << endl;

        cout << "Scheduling first send to t=5.0s\n";
        cMsg = new cMessage("tictocMsgObjectName");
        scheduleAt(5.0, event);
    }

}

void Node::handleMessage(cMessage *msg) {

    // There are several ways of distinguishing messages, for example by message kind (an int attribute of cMessage) or by class using dynamic_cast
    // (provided you subclass from cMessage). In this code we just check if we recognize the pointer, which (if feasible) is the easiest and fastest method.
    if (msg == event) {  // or if (msg->isSelfMessage())
        // The self-message arrived, so we can send out tictocMsg and nullptr out its pointer so that it doesn't confuse us later.
        cout << "getName() " << getName() << endl;
        cout << "Wait period is over, sending back message\n"<< endl;
        send(cMsg, "out");
        cMsg = nullptr;
    } else {
        // If the message we received is not our self-message, then it must be the tic-toc message arriving from our partner. We remember its
        // pointer in the cMsg variable, then schedule our self-message to come back to us in 1s simulated time.
        cout << "getName() " << getName() << endl;
        cout << "Message arrived, starting to wait 1 sec...\n" ;
        cMsg = msg;
        scheduleAt(simTime() + 1.0, event);
    }
}
