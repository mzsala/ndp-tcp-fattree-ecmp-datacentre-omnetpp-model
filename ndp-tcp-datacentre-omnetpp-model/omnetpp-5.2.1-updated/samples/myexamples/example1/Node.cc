// Node.cc

#include <omnetpp.h>
#include <string.h>

using namespace omnetpp;
using namespace std;

class Node: public omnetpp::cSimpleModule {

protected:
    // The following redefined virtual function holds the algorithm.
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

// The module class needs to be registered with OMNeT++
Define_Module(Node);

void Node::initialize() {
    // Initialize is called at the beginning of the simulation.
    // To bootstrap the Node1-Node2-Node1-Node2 process, one of the modules needs
    // to send the first message. Let this be  'Node1'.
    // getName(): Returns pointer to the object's name
    // Am I Node1 or Node2 ? (Select a node)
    if (strcmp("Node1", getName()) == 0) {
        // create and send first message on gate "out". "Node1tocMsg" is an
        // arbitrary string which will be the name of the message object.
        cMessage *msg = new cMessage("cMsg-0");
      //  Send  a message through the gate given with its name and index
        send(msg, "out");
    }
}

void Node::handleMessage(cMessage *msg) {
    // The handleMessage() method is called whenever a message arrives
    // at the module. Here, we just send it to the other module, through
    // gate 'out'. Because both `Node1' and 'Node2' does the same, the message
    // will bounce between the two.
   // delete msg;
   // cMessage *newmsg = new cMessage("cMsg-new");
   // send(newmsg, "out");// send out the message
    send(msg, "out");// send out the message
    EV << "Received message `" << msg->getName() << "', sending it out again\n";
    cout << "hiii " << endl;
}
