#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
using namespace std;
using namespace omnetpp;

class Node: public cSimpleModule {
protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(Node);

void Node::initialize() {
    if (getIndex() == 0) {
        cMessage *msg = new cMessage("MSG");
        scheduleAt(0.0, msg);
    }
}

void Node::handleMessage(cMessage *msg) {
    cout << "Node: getIndex() of the Node submodule=  " << getIndex() << endl;
    for (cModule::GateIterator i(this); !i.end(); i++) {
        cGate *myGatesIter = *i;
        cout << "  myGatesIter, getFullPath: " << myGatesIter->getFullPath()
                << " , gateFullName: " << myGatesIter->getFullName()
                << " , gateName: " << myGatesIter->getName()
                << " ,GATE getIndex: " << myGatesIter->getIndex()
                << " , getVectorSize: " << myGatesIter->getVectorSize()
                << " , getType: " << cGate::getTypeName(myGatesIter->getType())
                << endl;
    }
    send(msg, "out", 1);
}
