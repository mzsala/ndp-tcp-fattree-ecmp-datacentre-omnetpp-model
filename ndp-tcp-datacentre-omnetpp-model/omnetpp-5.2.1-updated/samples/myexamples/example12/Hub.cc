// Hub.cc
#include <omnetpp.h>
#include <iostream>

using namespace omnetpp;
using namespace std;

class Hub: public omnetpp::cSimpleModule {
protected:
    virtual void initialize();
    virtual void handleMessage(omnetpp::cMessage *msg);
    virtual void finish();

};

Define_Module(Hub);

void Hub::initialize() {
}

void Hub::handleMessage(omnetpp::cMessage *msg) {
    std::cout << std::endl << "Module getName:  " << getName() << std::endl;

    omnetpp::cGate *g = msg->getArrivalGate();
    cout << " ArrivalGate  " << g->getFullName() << endl;
    cout << " gates number at the Hub Module:  " << g->size() << endl;

    for (int i = 0; i < g->size(); i++) {
        if (i != g->getIndex()) {
            cMessage *copyMsg=msg->dup();
            send(copyMsg, "out", i);
            cout << "Hub's output gate:  " << i << endl;
            std::cout << "ArrivalModule " << copyMsg->getArrivalModule()->getFullName() << std::endl;
        }
    }
}

void Hub::finish() {
}
