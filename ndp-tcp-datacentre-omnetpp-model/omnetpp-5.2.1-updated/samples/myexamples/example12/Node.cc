// Node.cc
#include <omnetpp.h>
using namespace omnetpp;
class Node: public omnetpp::cSimpleModule {


protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
};

Define_Module(Node);

void Node::initialize() {
    if (cModule::getIndex() == 0) {
        cMessage *msg = new cMessage;
        scheduleAt(0.01, msg);
    }
}

void Node::handleMessage(cMessage *msg) {

    std::cout << std::endl << "Module getName:  " << getName() << " Index:"<< getIndex()<< std::endl;
     std::cout << " SenderModule " << msg->getSenderModule()->getFullName()<< std::endl;
     std::cout << "ArrivalModule " << msg->getArrivalModule()->getFullName() << std::endl;

    if (cModule::getIndex() == 0 ||  cModule::getIndex() == 4) {
        send(msg, "out");
    } else {
        delete (msg);
        std::cout << "I am Module " << getIndex()  << " deleting the received msg" << std::endl;
    }

}

