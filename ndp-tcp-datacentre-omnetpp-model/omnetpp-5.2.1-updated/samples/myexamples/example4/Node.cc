// Node.cc

#include <omnetpp.h>
#include <string.h>
using namespace std;
using namespace omnetpp;
class Node: public omnetpp::cSimpleModule {

private:
    int counter;  // Note the counter here

protected:
    // The following redefined virtual function holds the algorithm.
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

// The module class needs to be registered with OMNeT++
Define_Module(Node);

void Node::initialize() {

    // Initialize the counter with the "limit" module parameter, declared
    // in the NED file (Net.ned).
    counter = par("limit");

    // we no longer depend on the name of the module to decide
    // whether to send an initial message
    if (par("sendMsgOnInit").boolValue() == true) {
        cout << "getName() " << getName() << endl; // Node1
        cout << "Sending initial message\n";
        cMessage *msg = new cMessage("cMsgObject");
        send(msg, "out");
    }
}

void Node::handleMessage(cMessage *msg) {
    counter--;
    if (counter == 0) {
        cout << getName() << "'s counter reached zero, deleting message\n";
        delete msg;
    } else {
        cout << getName() << "'s counter is " << counter  << ", sending back message\n";
        send(msg, "out");
    }
}
