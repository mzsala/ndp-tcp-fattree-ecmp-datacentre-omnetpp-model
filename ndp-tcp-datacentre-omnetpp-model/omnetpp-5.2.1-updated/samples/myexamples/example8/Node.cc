#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
using namespace std;
using namespace omnetpp;

/**
 * Let's make it more interesting by using several (n) `node' modules,
 * and connecting every module to every other. For now, let's keep it
 * simple what they do: module 0 generates a message, and the others
 * keep tossing it around in random directions until it arrives at
 * module 3.
 */
class Node: public cSimpleModule {
protected:
    virtual void forwardMessage(cMessage *msg);
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(Node);

void Node::initialize() {

    cout << "getIndex()=  " << getIndex() << endl;
    if (getIndex() == 0) {
        // Boot the process scheduling the initial message as a self-message.
        char msgname[20];
        sprintf(msgname, "cMsg-%d", getIndex());
        cMessage *msg = new cMessage(msgname);
        scheduleAt(0.0, msg);
    }
}

void Node::handleMessage(cMessage *msg) {
    if (getIndex() == 3) {
        // Message arrived.
        cout << " @@@ This is your destination @@" << getName() << " " << getIndex() <<endl;
        cout << "Message " << msg << " arrived.\n";
        delete msg;
    } else {
        // We need to forward the message.
        forwardMessage(msg);
    }
}

void Node::forwardMessage(cMessage *msg) {
    // In this example, we just pick a random gate to send it on.
    // We draw a random number between 0 and the size of gate `out[]'.
    int n = gateSize("out");
    int k = intuniform(0, n - 1 );
    cout << getName() << " " << getIndex() << " #of gates in this node: gateSize( out )= " << gateSize("out") << endl;
    cout << "Forwarding message " << msg << " on port out[" << k << "]\n" << endl;
    send(msg, "out", k);
}
