#include <stdio.h>
#include <string.h>
#include <omnetpp.h>

#include "frame_m.h"
using namespace std;
using namespace omnetpp;



class Node: public cSimpleModule {
protected:
    virtual Frame *generateMessage();

    virtual void forwardMessage(Frame *msg);
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(Node);

void Node::initialize() {

    cout << "getIndex()=  " << getIndex() << endl;
    if (getIndex() == 0) { // Module 0 sends the first message
        // Boot the process scheduling the initial message as a self-message.
        Frame *msg = generateMessage();
        scheduleAt(0.0, msg);
    }
}

void Node::handleMessage(cMessage *msg) {

    Frame *ttmsg = check_and_cast<Frame *>(msg);
    if (ttmsg->getDestination() == getIndex()) {
        // Message arrived.
        bubble("ARRIVED, starting new one!");
        cout << " @@@ This is your destination @@" << getName() << " "
                << getIndex() << endl;
        cout << "Message " << ttmsg << " arrived.\n";
        delete ttmsg;

        // Generate another one.
        cout << "Generating another message: ";
        Frame *newmsg = generateMessage();
        cout << newmsg << endl;
        forwardMessage(newmsg);

    } else {
        // We need to forward the message.
        forwardMessage(ttmsg);
    }
}

void Node::forwardMessage(Frame *msg) {

    // Increment hop count.
        msg->setHopCount(msg->getHopCount()+1);
        cout << "msg->getHopCount() "<< msg->getHopCount() << endl;

    // In this example, we just pick a random gate to send it on.
    // We draw a random number between 0 and the size of gate `out[]'.
    int n = gateSize("gate");
    int k = intuniform(0, n - 1);
    cout << getName() << " " << getIndex()
            << " #of gates in this node: gateSize( out )= " << gateSize("gate")
            << endl;
    cout << "Forwarding message " << msg << " on port out[" << k << "]\n"
            << endl;
    send(msg, "gate$o", k);
}


Frame *Node::generateMessage()
{
    // Produce source and destination addresses.
    int src = getIndex();  // our module index
   // int n = getVectorSize();  // module vector size
    int dest = 3;

    if (src==dest) { dest= 0 ; }
    char msgname[20];
    sprintf(msgname, "Msg:from  %d  to  %d", src, dest);

    // Create message object and set source and destination field.
    Frame *msg = new Frame(msgname);
    msg->setSource(src);
    msg->setDestination(dest);
    return msg;
}
