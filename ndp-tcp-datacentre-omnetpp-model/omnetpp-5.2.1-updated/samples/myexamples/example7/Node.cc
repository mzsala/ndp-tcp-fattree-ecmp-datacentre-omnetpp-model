/*
 * Node.cc
 *
 *  Created on: 10 Nov 2016
 *      Author: ma777
 */
#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include <iostream>

using namespace omnetpp;
using namespace std;

class TxNode: public omnetpp::cSimpleModule {
private:
    simtime_t timeout;  // timeout
    cMessage *timeoutEvent;  // holds pointer to the timeout self-message
    int seq;  // message sequence number
    cMessage *message;  // message that has to be re-sent on timeout
public:
    TxNode();
    virtual ~TxNode();
protected:
    virtual cMessage *generateNewMessage();
    virtual void sendCopyOf(cMessage *msg);
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(TxNode);

TxNode::TxNode() {
    timeoutEvent = message = nullptr;
}

TxNode::~TxNode() {
    cancelAndDelete(timeoutEvent);
}

void TxNode::initialize() {
    // Initialize variables.
    seq = 0;
    timeout = 1.0; // 1 simulated time
    timeoutEvent = new cMessage("timeoutEvent");

    // Generate and send initial message.
    cout << getName() << " Sending initial message\n";
    message = generateNewMessage();
    sendCopyOf(message);
    scheduleAt(simTime() + timeout, timeoutEvent);
}

cMessage *TxNode::generateNewMessage() {
    // Generate a message with a different name every time.
    char msgname[20];
    sprintf(msgname, "MyMsg-%d", ++seq);
    cMessage *msg = new cMessage(msgname);
    return msg;
}

void TxNode::sendCopyOf(cMessage *msg) {
    // Duplicate message and send the copy.
    cout << getName() << " Duplicate message and send the copy " << msg->getName() << endl;
    cMessage *copy = (cMessage *) msg->dup();
    send(copy, "out");
}

void TxNode::handleMessage(cMessage *msg) {
    {
        if (msg == timeoutEvent) {
            // If we receive the timeout event, that means the packet hasn't
            // arrived in time and we have to re-send it.
            cout <<getName() << "Timeout expired, resending message and restarting timer\n";
            sendCopyOf(message);
            scheduleAt(simTime() + timeout, timeoutEvent);
        } else {  // message arrived
                  // Acknowledgement received
            cout << getName() << " Received: " << msg->getName() << "\n";
            cout << getName() << " Timer cancelled.\n" << endl;
            delete msg;

            // Also delete the stored message and cancel the timeout event.
            cancelEvent(timeoutEvent);
            delete message;

            // Ready to send another one.
            message = generateNewMessage();
            sendCopyOf(message);
            scheduleAt(simTime() + timeout, timeoutEvent);
        }
    }
}

/**
 * Sends back an acknowledgement -- or not.
 */
class RxNode: public cSimpleModule {
protected:
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(RxNode);

void RxNode::handleMessage(cMessage *msg) {
    if (uniform(0, 1) < 0.3) {
        cout << "\"@@@@@@@@@@@@@@@@@@ Losing\" message.\n";
        bubble("message lost");  // making animation more informative...
        delete msg;
    } else {
         cout << getName() << " received: " << msg
                << " msg , sending back an acknowledgement.\n";
        delete msg;
        send(new cMessage("ackMsg"), "out");
    }
}
