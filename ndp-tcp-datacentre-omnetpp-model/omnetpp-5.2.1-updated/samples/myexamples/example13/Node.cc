// Node.cc
#include <omnetpp.h>
#include <frame_m.h>

using namespace omnetpp;
using namespace std;

class Node: public omnetpp::cSimpleModule {
private:
    int no_sent;
    int no_rcvd;
    double time_interval;

    int source;
    int destination;

protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void finish();
};

Define_Module(Node);

void Node::initialize() {
    no_sent = 0;
    no_rcvd = 0;
    time_interval = par(time_interval);

    source = getIndex();
    destination = par("destination_address");

//    if (cModule::getIndex() == 0) {
        cMessage *msg = new cMessage;
        scheduleAt(0.01, msg);
//    }

}

void Node::handleMessage(cMessage *msg) {

    if (msg->isSelfMessage()) {
        // cMessage *out_msg = new cMessage;
        Frame *out_msg = new Frame();
        out_msg->setSource(source);
        out_msg->setDestination(destination) ;

        send(out_msg, "out");
        no_sent++;
          scheduleAt(simTime() + time_interval, msg);
    } else {

        Frame *in_msg=check_and_cast<Frame *>(msg);
        if(in_msg->getDestination()==source){
        no_rcvd++;
        }
        delete (msg);
    }
}

void Node::finish() {
    recordScalar("Number of recevied message ", no_rcvd);
    recordScalar("Number of sent message", no_sent);
}

//
//
