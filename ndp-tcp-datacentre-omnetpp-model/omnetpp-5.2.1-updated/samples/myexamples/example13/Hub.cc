// Hub.cc
#include <omnetpp.h>
#include <iostream>

using namespace omnetpp;
using namespace std;

class Hub: public omnetpp::cSimpleModule {
protected:
    virtual void initialize();
    virtual void handleMessage(omnetpp::cMessage *msg);
    virtual void finish();

};

Define_Module(Hub);

void Hub::initialize() {}

void Hub::handleMessage(omnetpp::cMessage *msg) {

    omnetpp::cGate *g = msg->getArrivalGate();
    cout << " getPathhhh  " << g->getFullPath() << endl;
    cout << " gates number at the Hub Module:  " << g->size() << endl;

    int i;
    for (i = 0; i < g->size(); i++) {
        if (i != g->getIndex())
            send(msg->dup(), "out", i);
    }
}

void Hub::finish() {}
