// Node.cc
#include <omnetpp.h>
using namespace omnetpp;
using namespace std;

class Node: public omnetpp::cSimpleModule {
private:
    int no_sent;
    int no_rcvd;
    double tt;

protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void finish();
};

Define_Module(Node);

void Node::initialize() {
    no_sent = 0;
    no_rcvd = 0;
    tt = par("time_interval");
    cout<<" Node getFullPath:: " << getFullPath() << endl;

    cout<<" time inteval " << tt << endl;
    cMessage *msg = new cMessage;
    scheduleAt(0.01, msg);
}

void Node::handleMessage(cMessage *msg) {

    if (msg->isSelfMessage()) {
        cMessage *out_msg = new cMessage;
        send(out_msg, "out");
        no_sent++;
        scheduleAt(simTime() + tt, msg);
    } else {
        no_rcvd++;
        delete (msg);
    }
}

void Node::finish() {
    recordScalar("Number of recevied message ", no_rcvd);
    recordScalar("Number of sent message", no_sent);
}


//
//
