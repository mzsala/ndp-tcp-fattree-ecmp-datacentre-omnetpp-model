
// Node.cc
#include <omnetpp.h>
#include <string.h>
using namespace std;
using namespace omnetpp;
class Node: public omnetpp::cSimpleModule {

private:
    int counter;  // Note the counter here

protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(Node);

void Node::initialize() {

    // Initialize counter to ten. We'll decrement it every time and delete
    // the message when it reaches zero.
    counter = 5;

    // The WATCH() statement below will let you examine the variable under Tkenv. After doing a few steps in the simulation,
    // double-click either
    // 'Node1' or 'Node2', select the Contents tab in the dialog that pops up, and you'll find "counter" in the list.
    WATCH(counter);

    cout << "getName()" << getName() << endl;

    if (strcmp("Node2", getName()) == 0) {
        cout << "Sending initial message\n";
        cout << getName() << endl;
        cMessage *msg = new cMessage(" cMsgObject");
        send(msg, "out");
    }
}

void Node::handleMessage(cMessage *msg) {
    counter--;
    if (counter == 0) {
        // If counter is zero, delete message. If you run the model, you'll find that the simulation will stop at this
        //  point with the message "no more events".
        cout << getName() << "'s counter reached zero, deleting message\n";
        delete msg;
    } else {
        // The handleMessage() method is called whenever a message arrives at the module. Here, we just send it to the other module,
        // through gate 'out'. Because both `Node1' and 'Node2' does the same, the message will bounce between the two.
        cout << getName() << "'s counter is " << counter
                << ", sending back message\n";
        cout << "Received message ' " << msg->getName()
                << "', sending it out again\n";
        send(msg, "out");    // send out the message
    }
}
