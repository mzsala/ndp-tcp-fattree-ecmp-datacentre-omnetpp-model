#include <stdio.h>
#include <string.h>
#include <omnetpp.h>
#include <iostream>

using namespace omnetpp;
using namespace std;

class TxNode: public omnetpp::cSimpleModule {
private:
    simtime_t timeout;  // timeout
    cMessage *timeoutEvent;  // holds pointer to the timeout self-message
public:
    TxNode();
    virtual ~TxNode();
protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(TxNode);

TxNode::TxNode() {
    timeoutEvent = nullptr;
}

TxNode::~TxNode() {
    cancelAndDelete(timeoutEvent);
}

void TxNode::initialize() {
    // Initialize variables.
    timeout = 1.0; // simulated time
    timeoutEvent = new cMessage("timeoutEvent");

    // Generate and send initial message.
    cout << "Sending initial message\n";
    cMessage *msg = new cMessage("cMsg");
    send(msg, "out");
    scheduleAt(simTime() + timeout, timeoutEvent);
}

void TxNode::handleMessage(cMessage *msg) {
    {
        if (msg == timeoutEvent) {
            // If we receive the timeout event, that means the packet hasn't
            // arrived in time and we have to re-send it.
            cout << "Timeout expired, resending message and restarting timer\n";
            cMessage *newMsg = new cMessage("cMsg");
            send(newMsg, "out");
            scheduleAt(simTime() + timeout, timeoutEvent);
        } else {  // message arrived
                  // Acknowledgement received -- delete the received message and cancel
                  // the timeout event.
            cout << getName() << " Timer cancelled.\n" << endl;
            cancelEvent(timeoutEvent);
            delete msg;
            // Ready to send another one.
            cout << getName() << " Ready to send another one" << endl;
            cMessage *newMsg = new cMessage("cMsg");
            send(newMsg, "out");
            scheduleAt(simTime() + timeout, timeoutEvent);
        }
    }
}


/**
 * Sends back an acknowledgement -- or not.
 */
class RxNode: public cSimpleModule {
protected:
    virtual void handleMessage(cMessage *msg) override;
};

Define_Module(RxNode);

void RxNode::handleMessage(cMessage *msg) {
    if (uniform(0, 1) < 0.3) {
        cout << "\"@@@@@@@@@@@@@@@@@@ Losing\" message.\n";
        bubble("message lost");  // making animation more informative...
        delete msg;
    } else {
        cout << getName() << " Sending back same message as acknowledgement.\n";
        send(msg, "out");
    }
}

