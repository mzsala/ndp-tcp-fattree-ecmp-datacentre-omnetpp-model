
function plotThroughput(K)

% path(path,genpath('/Volumes/LocalDataHD/m/ma/ma777/Desktop/distinguishable_colors'));
% lineStyles = {'-', '--', ':', '-.'};
% myColors = distinguishable_colors(nLines);
%%
k=K;
% numShortFlows =100;
% percentLongFlowNodes=0.33;
% kBytes= 70 ;
conf=dlmread(fullfile('..','MatConfig.csv'));
percentLongFlowNodes=conf(4); 
kBytes=conf(3)*1500;

%%
numServers= (k^3)/4;
numlongflowsRunningServers = floor(numServers * percentLongFlowNodes); 

nLines = length(kBytes);

for i=1:length(kBytes)
    a= num2str(kBytes(i)/1000);
    b=num2str(numServers);
    c=dlmread(fullfile('..','MatInstThroughput.csv'));
    r=sort(c);
    
%   r( find(r<(0.92*10^9)))=0.95*10^9;
%     r=sort(r);
   
    shortFlows= r(1:end-numlongflowsRunningServers);
%     plot(shortFlows,'linestyle', lineStyles{rem(i-1,numel(lineStyles))+1},...
%         'coalor', myColors(i,:),...
%         'linewidth',3);
    figure
     movegui('center');
     plot(shortFlows/10^9, 'r','LineWidt',2) 
     legendInfo{i} =  [ a, ' KB'];
    hold on
end

imTitle = ['Fat tree - #servers= ', num2str(numServers)];
legend(legendInfo)
set(gca,'fontsize',15)
% title(imTitle );
xlabel('Flow rank', 'FontSize',15)
ylabel('Goodput (Gbps)' ,'FontSize',15)
grid
%  ax = gca;
%  ax.YAxis.Exponent = 1;
%  ax.XAxis.Exponent = 0;
ylim([0 1])
xlim([1 length(shortFlows)])


% set(gca,'fontsize',25)
% set(gca,'FontName','Times')
% savefig('src.fig')
%   h1 =openfig('src.fig')
% set(h1,'Units','Inches');
% pos = get(h1,'Position');
% set(h1,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
% saveas(h1,'goodput.pdf')

end

